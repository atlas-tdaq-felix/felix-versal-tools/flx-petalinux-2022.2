# Petalinux project for FELIX FLX-182 board

This repository includes files needed to build the SD card image to boot FLX-182 board. This project was prepared with Petalinux 2024.1. The firmware project used to generate the `.xsa` file is located [here](https://gitlab.cern.ch/atlas-tdaq-felix/firmware).

## Requirements

To build the image, you'll need ~30 GB of free disk drive space. You can use the Petalinux build container with Docker (recommended) or you can setup Petalinux on your system. The build container repository can be found [here](https://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci).

### Docker on Windows

- Install [Git for windows](https://git-scm.com/download/win)
- Install WSL by following the [instructions on Microsoft website](https://learn.microsoft.com/en-us/windows/wsl/install) from "Install WSL command" section
- Install [Docker Desktop for Windows](https://docs.docker.com/desktop/install/windows-install/). Follow the instructions for WSL 2 Backend installation.

### Docker on Linux

Make sure `git` is installed in your system, e.g. by running
```
sudo apt-get install git
```

Install docker by following the [official installation instructions](https://docs.docker.com/engine/install/#server).

### Native installation

You can download Petalinux from the [Xilinx website](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools/2024-1.html).

Please follow the [Reference Guide (UG1144)](https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide/Setting-Up-Your-Environment) for the list of the compatible systems and the installation instructions.


## Downloading the project files

Clone the project files:
```
# using https
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/flx-petalinux.git


# using ssh
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/felix-versal-tools/flx-petalinux.git
```

If you will build using the docker container, also pull the container:
```
docker pull gitlab-registry.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci
```


## Building with the Docker container

Open the terminal (PowerShell if using Windows). Launch the container and mount the directory containing the project file to it:

```
cd flx-petalinux/
docker run -it --rm -v $(pwd):/home/petalinux/build gitlab-registry.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci
```
Note: if you are using podman (docker replacement on RHEL) add the flag `--privileged`. Also make sure the `flx-petalinux` is writeable by other users.
Inside the container, use these commands to build the project:
```
petalinux-build
petalinux-package --boot --u-boot --force
petalinux-package --wic
```
The resulting SD card image will be saved to `Path_to_the_project_root_directory/images/linux`

## Building natively

Open the terminal and run these commands:

```
source Path_to_your_Petalinux_installation/settings.sh
cd Path_to_the_project_root_directory
petalinux-build
petalinux-package --boot --u-boot --force
petalinux-package --wic
```

The resulting SD card image will be saved to `Path_to_the_project_root_directory/images/linux` and `/tftpboot` (if you have `tftpboot` installed and configured).

## Flashing the SD card on Linux

### Partitioning and formatting the SD card

If you have a blank card, it must be first partitioned and formatted.

Insert the SD card and run `dmesg | tail` to find the SD card device node (`/dev/sdd` in this example):

```
$ dmesg | tail
[ 9147.485994] sd 8:0:0:1: [sdd] 31116288 512-byte logical blocks: (15.9 GB/14.8 GiB)
[ 9147.486659] sd 8:0:0:1: [sdd] Write Protect is off
[ 9147.486662] sd 8:0:0:1: [sdd] Mode Sense: 21 00 00 00
[ 9147.487373] sd 8:0:0:1: [sdd] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
[ 9147.488923] sd 8:0:0:0: [sdc] Attached SCSI removable disk
[ 9147.701615]  sdd: sdd1 sdd2
[ 9147.703764] sd 8:0:0:1: [sdd] Attached SCSI removable disk
[ 9148.965296] FAT-fs (sdd1): Volume was not properly unmounted. Some data may be corrupt. Please run fsck.
[ 9149.008757] EXT4-fs (sdd2): recovery complete
[ 9149.010660] EXT4-fs (sdd2): mounted filesystem with ordered data mode. Opts: (null). Quota mode: none.
```

**/DEV/SDD IS AN EXAMPLE ONLY! MAKE ABSOLUTELY SURE YOU ARE USING THE CORRECT DEVICE FOR THE COMMANDS IN THIS SECTION! IF YOU DON'T YOU MIGHT DESTROY FILES ON YOUR COMPUTER!**

Partition the SD card using fdisk. Type `m` to print help, `p` to print the partition table, `n` to add a new partition, `d` to delete an existing partition, `w` to save changes to the disk and exit.

Create two primary partitions. Partition 1 size should be 256-1024 MB, the rest should be taken by partition 2:

```
$ sudo fdisk /dev/sdd
[sudo] password for lena:
 
Welcome to fdisk (util-linux 2.34).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
 
Command (m for help): p
 
Disk /dev/sdd: 14.86 GiB, 15931539456 bytes, 31116288 sectors
Disk model: MassStorageClass
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x5dac1493
 
Device     Boot  Start      End  Sectors  Size Id Type
/dev/sdd1  *      2048   526335   524288  256M 83 Linux
/dev/sdd2       526336 31116287 30589952 14.6G 83 Linux
```

Type `w` to save changes to the disk and exit.

If you receive the warning message `Re-reading the partition table failed.: Device or resource busy`, eject the SD card, pull it out if the computer and re-insert it to reload the partition table.

Format the new partitions:
```
sudo mkfs.fat /dev/sdd1
sudo mkfs.ext4 /dev/sdd2
```

Copy the following files to the smaller `fat` partition (`/dev/sdd1`):
```
BOOT.BIN  # contains Versal hardware configuration and bootloaders
boot.scr  # boot script for U-Boot
image.ub  # Linux kernel, initramfs and the device tree
```

Extract the root filesystem to the `ext4` partition (`/dev/sdd2`):
```
cd Path_to_your_SD_card_mounting_point
sudo cpio -idv < /tftpboot/rootfs.cpio
```

You can find the path to your SD card mounts by running the command:
```
mount | grep /dev/sdd
```

When finished, eject the SD card:
```
sudo eject /dev/sdd
```

## Flashing the SD card on Windows

You can flash the `.wic` image found in `Path_to_the_project_root_directory/images/linux` (after you build the project) using [BalenaEtcher](https://www.balena.io/etcher) tool.


## Flashing the QSPI memory

You can flash the QSPI memory by first preparing the SD card image and booting into the U-Boot bootloader. The bootloader provides the functionality to flash the QSPI.

**Before you begin, make sure that the Petalinux project is configured correctly for booting from the QSPI flash. Here is [a detailed tutorial from Xilinx](https://xilinx.github.io/Embedded-Design-Tutorials/docs/2022.2/build/html/docs/Introduction/Versal-EDT/docs/4-boot-and-config.html#boot-sequence-for-qspi-boot-mode) on how to configure and boot Petalinux from QSPI.**

Connect the UART terminal to the FLX-182 board. Interrupt the boot at the U-Boot stage when the countdown message appears (`Hit any key to stop autoboot: 3`)

Use the following commands to flash the QSPI:

```
# erase the flash
sf probe 0 0 0
sf erase 0 10000000
 
# retrieve and flash BOOT.BIN
fatload mmc 0 0x80000 BOOT.BIN
sf write 0x80000 0x0 ${filesize}
 
# retrieve and flash Linux fit image
fatload mmc 0 0x80000 image.ub
sf write 0x80000 0x3220000 ${filesize}
 
# retrieve and flash boot script
fatload mmc 0 0x80000 boot.scr
sf write 0x80000 0x3200000 ${filesize}
```

Currently this Petalinux project uses `switch_root` to boot. In this scenario, `image.ub` only contains a small initial filesystem called `initramfs`, which is later replaced by the full filesystem stored on the SD card. As of the time of writing of this tutorial, the full filesystem is too large to fit into the QSPI flash uncompressed.

However, it is possible to fit the full filesystem by compressing it and packing it into the `image.ub` instead. In this case, the filesystem will only live in DRAM and any changes will be lost when FLX-182 reboots.

[This Xilinx tutorial](https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide/Configuring-INITRAMFS-Boot) describes how to configure either of these boot methods.


## Updating the `.XSA` file

To use the updated firmware in your image:

- Build your project and generate the device image
- Create the `.xsa` file by clicking `File->Export->Export Hardware...`, then select `Include device image`

Import the new `.xsa` file into the project:

```
petalinux-config --silentconfig --get-hw-description Your_XSA_File.xsa 
```

Follow the project building instructions to rebuild and re-package the image. If you didn't make any other changes to the project, you only need to rebuild the kernel:

```
petalinux-build -c kernel
petalinux-package --boot --u-boot --force
petalinux-package --wic
```

In this case, you only need to update these files on the SD card:
```
BOOT.BIN  # contains Versal hardware configuration and bootloaders
image.ub  # Linux kernel, initramfs and the device tree
```

## Updating the device tree

Device tree describes peripheral devices available to the Versal processing system. This includes on-board controllers (Ethernet, GPIO, eMMC), as well as the chips located on the board (INA226, TCA6424, TMP435), as well as any AXI peripherals instantiated in FPGA fabric.

If you want to make changes to the device tree, you need to edit `project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi`

If you want to view device tree entries auto-generated by the Xilinx tools and the include files, see `components/plnx_workspace/device-tree/device-tree` (these files will be generated when the project is built)

## Changing the MAC address

As of now, the MAC address is specified in `system-user.dtsi` in `&gem0` node.

The downside of this approach is that the image must be rebuilt to assign different MAC address to different FLX-182 boards. The standard approach is to instead have the bootloader read the MAC address from an I2C or QSPI memory. The issue with missing rst_b pull-up for the QSPI memory must be fixed.

Here are the instructions on the MAC handoff process for U-Boot with Zynq: https://tuxengineering.com/blog/2020/09/16/U-Boot-handling-MAC-address.html


## Further instructions

Please use these links for Petalinux documentation and tutorials:

- https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide
- https://xilinx.github.io/Embedded-Design-Tutorials/docs/2022.2/build/html/docs/Introduction/Versal-EDT/Versal-EDT.html
- https://xilinx-wiki.atlassian.net/wiki/spaces/A/overview?homepageId=18844350
