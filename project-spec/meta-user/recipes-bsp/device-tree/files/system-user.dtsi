#include "include/dt-bindings/gpio/gpio.h"

/include/ "system-conf.dtsi"
/ {

	aliases {
		serial0 = &serial0;
		ethernet0 = &gem0;
		ethernet1 = &gem1;
		i2c0 = &i2c0;
		i2c1 = &i2c1;
		i2c2 = &i2c2; // I2C_PMC
		mmc0 = &sdhci1;
		spi0 = &qspi;
		usb0 = &usb0;
		rtc0 = &rtc;
	};


    xtal48a: xtal_X3 {
        compatible = "fixed-clock";
        #clock-cells = <0>;
        clock-frequency = <48000000>;
        clock-accuracy = <10>;
    };


    xtal48b: xtal_X2 {
        compatible = "fixed-clock";
        #clock-cells = <0>;
        clock-frequency = <48000000>;
        clock-accuracy = <10>;
    };


    wrclfref: xtal_U87 {
        compatible = "fixed-clock";
        #clock-cells = <0>;
        clock-frequency = <125000000>;
        clock-accuracy = <20>;
    };


    // TODO: route this clock to the XPIO pins AW19, AY18
    xpioa: xpio_a {
        compatible = "fixed-clock";
        #clock-cells = <0>;
        clock-frequency = <40000000>;
        clock-accuracy = <10>;
    };


    // TODO: route this clock to the XPIO pins BB16, BC16
    xpiob: xpio_b {
        compatible = "fixed-clock";
        #clock-cells = <0>;
        clock-frequency = <40000000>;
        clock-accuracy = <10>;
    };

    v12p0: regulator {
        compatible = "regulator-fixed";
        reg = <0>;
        regulator-name = "12v_input_fixed";
        regulator-always-on;
        regulator-min-microvolt = <12000000>;
        regulator-max-microvolt = <12000000>;
    };


};

// Keep RST_B pins on the QSPI high
// hopefully GPIO initializes before the QSPI
&axi_gpio_0 {
    qspi1_rst_hog {
        gpio-hog;
        gpios = <17 0>;
        output-high;
        line-name = "qspi1_rst_gpio";
    };

    qspi_rst_hog {
        gpio-hog;
        gpios = <18 0>;
        output-high;
        line-name = "qspi_rst_gpio";
    };

    qspi0_rst_hog {
        gpio-hog;
        gpios = <19 0>;
        output-high;
        line-name = "qspi0_rst_gpio";
    };
};


&dcc {
	status = "okay";
};

&sdhci1 { /* PMC_MIO26-36/51 */
	xlnx,mio-bank = <1>;
	no-1-8-v;
};


&gem0 { /* PMC_MIO_48, LPD_MIO0-11/24/25 */
	phy-handle = <&phy1>; /* u198 */
	phy-mode = "rgmii-id";
	local-mac-address = [ 00 0A 35 07 DE B2 ];
	mdio: mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		phy1: ethernet-phy@1 {
			#phy-cells = <1>;
			compatible = "ethernet-phy-id2000.a231";
			reg = <1>;
			ti,rx-internal-delay = <0xb>;
			ti,tx-internal-delay = <0xa>;
			ti,fifo-depth = <1>;
			ti,dp83867-rxctrl-strap-quirk;
			reset-assert-us = <100>;
			reset-deassert-us = <280>;
			reset-gpios = <&gpio1 48 GPIO_ACTIVE_LOW>;
		};

	};
};


&qspi {
	flash@0 {
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "mt25qu02g", "jedec,spi-nor"; 
		reg = <0>;
	};
};

&amba {
    flxnet_instance: flxnet@20100030000 {
        #address-cells = <0x02>;
        #size-cells = <0x02>;
        reg = <0x201 0x00030000 0x00 0x100>;        
        compatible = "atlas_tdaq_felix,VERSAL_virtual_network_device_v1.0";
        status = "okay";
    };
};



&axi_iic_0 {
    // i2c-3
    // clock-frequency = <400000>;
    // don't reconfigure the clock, it's using the PL0 clock
    // shared by the AXI bus peripherals

	i2c-mux@77 {
		#address-cells = <0x01>;
        #size-cells = <0x00>;
        compatible = "nxp,pca9548";
        reg = <0x77>;

        i2c@0 {
            // i2c-4
        	#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x00>;

			ltm4700@4e {
				compatible = "lltc,ltm4700";
				reg = <0x4e>;
                status = "okay";
				regulators {
					vout0 {
						regulator-name = "VCCINT_VOUT0";
                        regulator-always-on;
                        regulator-boot-on;
                        vin-supply = <&v12p0>;
					};
					vout1 {
						regulator-name = "VCCINT_VOUT1";
                        regulator-always-on;
                        regulator-boot-on;
                        vin-supply = <&v12p0>;
					};
				};
			};

        	ina226@40 {
                #io-channel-cells = <0x01>;
                label = "12P0V";
                compatible = "ti,ina226";
                reg = <0x40>;
                shunt-resistor = <0x2710>;
            };

            ina226@41 {
                #io-channel-cells = <0x01>;
                label = "VCCINT";
                compatible = "ti,ina226";
                reg = <0x41>;
                shunt-resistor = <0x0FA>;
            };

            ina226@42 {
                #io-channel-cells = <0x01>;
                label = "MGTAVCC";
                compatible = "ti,ina226";
                reg = <0x42>;
                shunt-resistor = <0x3E8>;
            };

            ina226@43 {
                #io-channel-cells = <0x01>;
                label = "MGTAVTT";
                compatible = "ti,ina226";
                reg = <0x43>;
                shunt-resistor = <0x3E8>;
            };

        	ina226@44 {
                #io-channel-cells = <0x01>;
                label = "SYS12";
                compatible = "ti,ina226";
                reg = <0x44>;
                shunt-resistor = <0x3E8>;
            };


            ina226@45 {
                #io-channel-cells = <0x01>;
                label = "SYS15";
                compatible = "ti,ina226";
                reg = <0x45>;
                shunt-resistor = <0x3E8>;
            };

            ina226@46 {
                #io-channel-cells = <0x01>;
                label = "SYS18";
                compatible = "ti,ina226";
                reg = <0x46>;
                shunt-resistor = <0x3E8>;
            };

            ina226@47 {
                #io-channel-cells = <0x01>;
                label = "SYS25";
                compatible = "ti,ina226";
                reg = <0x47>;
                shunt-resistor = <0x3E8>;
            };

            ina226@48 {
                #io-channel-cells = <0x01>;
                label = "SYS33";
                compatible = "ti,ina226";
                reg = <0x48>;
                shunt-resistor = <0x3E8>;
            };

            ina226@49 {
                #io-channel-cells = <0x01>;
                label = "SYS38";
                compatible = "ti,ina226";
                reg = <0x49>;
                shunt-resistor = <0x3E8>;
            };            
    	};

    	i2c@1 {
            // i2c-5
    		#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x01>;

            // supported by drivers/hwmon/tmp401.c
            // label attribute is not used by driver, but is read by the test webapp
			tmp435@48 {
				compatible = "ti,tmp435";
				reg = <0x48>;
				label = "MGTAVCC_TMP";
			};

			tmp435@49 {
				compatible = "ti,tmp435";
				reg = <0x49>;
				label = "MGTAVTT_TMP";
			};

			tmp435@4a {
				compatible = "ti,tmp435";
				reg = <0x4a>;
				label = "SYS12_TMP";
			};

			tmp435@4b {
				compatible = "ti,tmp435";
				reg = <0x4b>;
				label = "SYS15_TMP";
			};

			tmp435@4c {
				compatible = "ti,tmp435";
				reg = <0x4c>;
				label = "SYS18_TMP";
			};

			tmp435@4d {
				compatible = "ti,tmp435";
				reg = <0x4d>;
				label = "SYS33_TMP";
			};

			tmp435@4e {
				compatible = "ti,tmp435";
				reg = <0x4e>;
				label = "LTM4642_TMP";
			};
    	};


    	i2c@2 {
            // i2c-6
    		#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x02>;

    		adm1066@34 {
				compatible = "adi,adm1066";
				reg = <0x34>;
				label = "U68";
			};

			adm1066@35 {
				compatible = "adi,adm1066";
				reg = <0x35>;
				label = "U69";
			};

            // this clock should remain disabled because the system will freeze
            // if it tries to adjust it
    		clock-generator@60 {
                clock-output-names = "si570_ddr4_clk";
                factory-fout = <0xBEBC200>;
                #clock-cells = <0x00>;
                clock-frequency = <0xBEBC200>;
                compatible = "silabs,si570";
                status = "disabled";
                reg = <0x60>;
                temperature-stability = <0x60>;
            };
    	};


    	i2c@3 {
            // i2c-7
    		#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x03>;

            // DDR4 I2C

    		SI5345A_ref_40: clock-generator@60 {
                clock-output-names = "si570_sys_clk_40_for_SI5345A";
                factory-fout = <0x2639280>;
                #clock-cells = <0x00>;
                clock-frequency = <0x2639280>;
                compatible = "silabs,si570";
                status = "disabled";
                reg = <0x60>;
                temperature-stability = <0x60>;
            };

			si53156@6b {
				compatible = "silabs,si53156";
				clock-output-names = "pcie_ref_clk";
				reg = <0x6b>;
			};

			clock-generator@55 {
                clock-output-names = "si570_sys_clk_100";
                factory-fout = <0x5F5E100>;
                #clock-cells = <0x00>;
                clock-frequency = <0x5F5E100>;
                compatible = "silabs,si570";
                status = "disabled";
                reg = <0x55>;
                temperature-stability = <0x60>;
            };

            temp-sensor@1b {
                compatible = "jedec,jc-42.4-temp";
                reg = <0x1b>;
                label = "DRAM temperature sensor";
            };
    	};


    	i2c@4 {
            // i2c-8
    		#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x04>;

    		// Samtec FireFly at J30, J31

    		SI5345B_ref_40: clock-generator@60 {
                clock-output-names = "si570_sys_clk_40_for_SI5345B";
                factory-fout = <0x2639280>;
                #clock-cells = <0x00>;
                clock-frequency = <0x2639280>;
                compatible = "silabs,si570";
                status = "disabled";
                reg = <0x60>;
                temperature-stability = <0x60>;
            };
    	};


    	i2c@5 {
            // i2c-9
    		#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x05>;

    		// Samtec FireFly at J27, J28

    		clock-generator@60 {
                clock-output-names = "si570_lti_ref_clk";
                factory-fout = <0xE555790>;
                #clock-cells = <0x00>;
                clock-frequency = <0xE555790>;
                compatible = "silabs,si570";
                status = "disabled";
                reg = <0x60>;
                temperature-stability = <0x60>;
            };

            clock-generator@55 {
            	compatible = "silabs,si569";
            	reg = <0x55>;
            	clock-output-names = "wrclk_dmtd";
            };
    	};


    	i2c@6 {
            // i2c-10
    		#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x06>;

    		// Samtec FireFly at J29

    		clock-generator@60 {
                clock-output-names = "si570_100G_ref_clk";
                factory-fout = <0x13356219>;
                #clock-cells = <0x00>;
                clock-frequency = <0x13356219>;
                compatible = "silabs,si570";
                status = "disabled";
                reg = <0x60>;
                temperature-stability = <0x60>;
            };

            SI5345A_ref_wrclk: clock-generator@55 {
            	compatible = "silabs,si569";
            	reg = <0x55>;
            	clock-output-names = "wrclk_ref";
            };
    	};


    	i2c@7 {
            // i2c-11
    		#address-cells = <0x01>;
            #size-cells = <0x00>;
            reg = <0x07>;


            clock-generator@5d {
                clock-output-names = "si570_ps_ref_clk";
                factory-fout = <0x1CED995>;
                #clock-cells = <0x00>;                
                clock-frequency = <0x1CED995>;
                compatible = "silabs,si570";
                status = "disabled";
                reg = <0x5d>;
                temperature-stability = <0x60>;
            };

            // U46
            si5345a: clock-generator@68 {
            	compatible = "silabs,si5345";
                status = "disabled";
                reg = <0x68>;
                #clock-cells = <2>;
                #size-cells = <0>;
                #address-cells = <1>;
                clocks = <&xtal48a>, <&wrclfref>, <&SI5345A_ref_40>, <&xpioa>;
                clock-names = "xtal", "in0", "in1", "in2", "in3";

                si5345a8: out@8 {
                    reg = <8>;
                };
            };

            // U45
            si5345b: clock-generator@69 {
            	compatible = "silabs,si5345";
                status = "disabled";
                reg = <0x69>;
                #clock-cells = <2>;
                #size-cells = <0>;
                #address-cells = <1>;
                clocks = <&xtal48b>, <&si5345a8>, <&SI5345B_ref_40>, <&xpiob>;
                clock-names = "xtal", "in0", "in1", "in2", "in3";

            };

            gpio@22 {
	            compatible = "ti,tca6424";
                status = "okay";
	            reg = <0x22>;
	            gpio-controller;
	            #gpio-cells = <2>;
	            gpio-line-names = "alert_sys12", "alert_vcc_int", "sys12_temp_alert_b",
	            	"sys12_temp_err_b", "sys15_temp_alert_b", "alert_sys15", "alert_sys33",
	            	"alert_sys38", "sys33_temp_alert_b", "sys33_temp_err_b", "ltm4700_alert",
	            	"vcc_int_fault", "sys18_temp_err_b", "alert_sys18", "sys18_temp_alert_b",
	            	"alert_sys25", "alert_mgtavcc", "mgtavcc_temp_alert_b", "mgtavcc_temp_err_b",
	            	"alert_12p0v", "mgtavtt_temp_err_b", "mgtavtt_temp_alert_b", "alert_mgtavtt",
	            	"sys15_tmp_err_b";
	        };

	        gpio@23 {
	            compatible = "ti,tca6424";
                status = "okay";
	            reg = <0x23>;
	            gpio-controller;
	            #gpio-cells = <2>;
	            gpio-line-names = "ff1_rx_rst_b", "ff1_rx_intr_b", "ff1_rx_select_b", "ff1_rx_prsnt_b",
	            	"ff1_tx_rst_b", "ff1_tx_intr_b", "ff1_tx_select_b", "ff1_tx_prsnt_b",
	            	"ff2_rx_rst_b", "ff2_rx_intr_b", "ff2_rx_select_b", "ff2_rx_prsnt_b",
	            	"ff2_tx_rst_b", "ff2_tx_intr_b", "ff2_tx_select_b", "ff2_tx_prsnt_b",
	            	"ff3_rst_b", "ff3_intr_b", "ff3_select_b", "ltm4642_temp_alt_b", "ltm4642_temp_err_b",
	            	"versal_cfg_done_gpio", "i2c_gpio_por", "versal_error_out_gpio";
	        };
		};

	};

};
