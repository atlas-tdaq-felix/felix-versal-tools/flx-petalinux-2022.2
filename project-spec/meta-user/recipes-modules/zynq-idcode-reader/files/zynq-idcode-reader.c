/*  zynq-idcode-reader.c - The simplest kernel module.

* Copyright (C) 2013 - 2016 Xilinx, Inc
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.

*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program. If not, see <http://www.gnu.org/licenses/>.

*/
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/sysfs.h>
#include<linux/kobject.h> 

#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

#ifdef CONFIG_ZYNQMP_FIRMWARE
#include <linux/firmware/xlnx-zynqmp.h>
#endif


/* Standard module information, edit as appropriate */
MODULE_LICENSE("GPL");
MODULE_AUTHOR
    ("Elena Zhivun <ezhivun@bnl.gov>");
MODULE_DESCRIPTION
    ("zynq-idcode-reader - Read IDCODE from ZYNQMP driver and expose it in sysfs");

#define DRIVER_NAME "zynq-idcode-reader"
#define DEVICE_NAME "zynqmp_idcode_device"
#define CLASS_NAME "zynqmp_idcode_class"
#define KOBJ_NAME "zynqmp_idcode"

static struct zynq_idcode {
	u32 idcode;
	u32 version;
	struct kobject *kobj;
	bool invalid_platform;
} data;


//static struct zynq_idcode data;


ssize_t idcode_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
	if (data.invalid_platform) {
		return sprintf(buf, "Unsupported platform!");
	} else {
		return sprintf(buf, "%x", data.idcode);
	}
}


ssize_t version_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
	if (data.invalid_platform) {
		return sprintf(buf, "Unsupported platform!");
	} else {
		return sprintf(buf, "%x", data.version);
	}	
}


static struct kobj_attribute idcode_attr = __ATTR(idcode, 0444, idcode_show, NULL);
static struct kobj_attribute version_attr = __ATTR(version, 0444, version_show, NULL);

static struct attribute *main_attrs[] = {
	&idcode_attr.attr,
	&version_attr.attr,
	NULL,
};

static const struct attribute_group main_attr_group = {
	.attrs = main_attrs,
};

static int __init zynq_idcode_reader_init(void)
{
	int ret;	

#ifdef CONFIG_ZYNQMP_FIRMWARE
	ret = zynqmp_pm_get_chipid(&(data.idcode), &(data.version));
	if (ret < 0) {
		printk(KERN_ERR "Cound not retrieve the IDCODE from ZynqMP\n");
		goto error_get_chipid;
	}

	data.invalid_platform = false;
#else
	data.invalid_platform = true;
#endif


	data.kobj = kobject_create_and_add(KOBJ_NAME, NULL);
	if (!data.kobj) {
		ret = -ENOMEM;
		printk(KERN_ERR "Cound not create the kobject\n");
		goto error_create_kobj;
	}

	ret = sysfs_create_group(data.kobj, &main_attr_group);
	if (ret) {
		printk(KERN_ERR "Cound not create the kobject attributes\n");
		goto error_create_attributes;
	}

	printk(KERN_INFO "Loaded zynq-idcode-reader driver\n");
	return 0;


error_create_attributes:
	kobject_put(data.kobj);

error_create_kobj:

#ifdef CONFIG_ZYNQMP_FIRMWARE
error_get_chipid:
#endif

	return ret;
}


static void __exit zynq_idcode_reader_exit(void)
{
	sysfs_remove_group(data.kobj, &main_attr_group);
	kobject_put(data.kobj);
	printk(KERN_INFO "Unloaded zynq-idcode-reader driver\n");
}

module_init(zynq_idcode_reader_init);
module_exit(zynq_idcode_reader_exit);
