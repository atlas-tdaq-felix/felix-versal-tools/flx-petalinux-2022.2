SUMMARY = "Recipe for build an external flxnet Linux kernel module"
SECTION = "PETALINUX/modules"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM ?= "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"
SRCREV = "2782598d1aee3c4ed8f37c4b221cd1e7526c6e9d"
#BB_STRICT_CHECKSUM = "0"
inherit module

INHIBIT_PACKAGE_STRIP = "1"

SRC_URI = "git://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/flxnet-drivers.git;protocol=https;branch=master"

S = "${WORKDIR}/git/network_device"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.
