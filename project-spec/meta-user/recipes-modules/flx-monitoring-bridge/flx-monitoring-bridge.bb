SUMMARY = "Installs systemd timer and script to pass DNA and SYSMON values to IP core"
SECTION = "PETALINUX/modules"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=12f884d2ae1ff87c09e5b7ccc2c4ca7e"

inherit systemd

S = "${WORKDIR}"

SRC_URI = "file://flx-monitoring-bridge.sh \
           file://flx-monitoring-bridge.service \
           file://flx-monitoring-bridge.timer \
           file://COPYING \
          "
RDEPENDS:${PN} = "bash"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} += "flx-monitoring-bridge.service"
SYSTEMD_SERVICE:${PN} += "flx-monitoring-bridge.timer"
SYSTEMD_AUTO_ENABLE:${PN} = "enable"

do_install:append() {
        install -d ${D}/${bindir}
        install -d ${D}${systemd_system_unitdir}
        install -m 0755 flx-monitoring-bridge.service ${D}${systemd_system_unitdir}
        install -m 0755 flx-monitoring-bridge.timer ${D}${systemd_system_unitdir}
        install -m 0755 flx-monitoring-bridge.sh ${D}/${bindir}
}

FILES:${PN} += "${systemd_system_unitdir}/flx-monitoring-bridge.service"
FILES:${PN} += "${systemd_system_unitdir}/flx-monitoring-bridge.timer"

