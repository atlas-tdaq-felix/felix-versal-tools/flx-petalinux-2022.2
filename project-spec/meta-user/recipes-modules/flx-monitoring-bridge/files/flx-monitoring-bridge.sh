#!/bin/bash
#DNA[31..0]
devmem 0x20100040000 32 $(devmem 0xF1250020 32)
#DNA[63..32]
devmem 0x20100040004 32 $(devmem 0xF1250024 32)
#VCCINT
devmem 0x20100040008 32 $(echo $(cat /sys/bus/iio/devices/iio\:device0/in_voltage6_vccint_input) 4096 3 | awk '{printf "%4.0f", $1*$2/$3}')
#VCCAUX
devmem 0x2010004000C 32 $(echo $(cat /sys/bus/iio/devices/iio\:device0/in_voltage7_vccaux_input) 4096 3 | awk '{printf "%4.0f", $1*$2/$3}')
#VCCBRAM
devmem 0x20100040010 32 $(echo $(cat /sys/bus/iio/devices/iio\:device0/in_voltage4_vcc_ram_input) 4096 3 | awk '{printf "%4.0f", $1*$2/$3}')
#TEMP
devmem 0x20100040014 32 $(echo $(cat /sys/bus/iio/devices/iio\:device0/in_temp160_temp_input) 273.8195 4096 502.9098 | awk '{printf "%4.0f", ($1+$2)*$3/$4}') 
