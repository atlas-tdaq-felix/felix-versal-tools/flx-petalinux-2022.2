SUMMARY = "Systemd service script for ez-hw-test"
LICENSE = "CLOSED"
  
SRC_URI = "file://ez_hw_test.service"
  
S = "${WORKDIR}"
  
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
  
inherit systemd
  
  
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "ez_hw_test.service"
SYSTEMD_AUTO_ENABLE:${PN} = "enable"
  
do_install() {
        install -d ${D}${systemd_system_unitdir}
        install -m 0644 ${WORKDIR}/ez_hw_test.service ${D}${systemd_system_unitdir}
}
  
# FILES:${PN} += "${@bb.utils.contains('DISTRO_FEATURES','sysvinit','${sysconfdir}/*', '', d)}"
FILES:${PN} += "${systemd_system_unitdir}/ez_hw_test.service"