SUMMARY = "Web application for automated hardware testing"
DESCRIPTION = "ez-hw-test is a Flask-based web application developed \
for testing custom hardware platforms for the Omega Group in \
Brookhaven National Laboratory, as part of the FELIX phase2 upgrade"
LICENSE = "CLOSED"

RDEPENDS:${PN} = " \
        ez-hw-test-init \
        python3-flask \
        python3-flask-restful \
        python3-bootstrap-flask \
        python3-flask-wtf \
        python3-wtforms \
        python3-werkzeug \
        python3-jinja2 \
        python3-markupsafe \
        python3-itsdangerous \
        python3-twisted \
        python3-gevent \
        python3-matplotlib \
        python3-pillow \
        python3-periphery \        
        python3-chipscopy \
        python3-unittest \
        python3-pytest \
        python3-pydoc \
        python3-loguru \
        python3-requests \
        iperf3 \
        memtester \
        mtd-utils \        
"


#SRC_URI = "git://git@gitlab.cern.ch:7999/atlas-tdaq-felix/felix-versal-tools/flx-181-webapp.git;protocol=ssh"
SRC_URI = "git://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/flx-181-webapp.git;protocol=https;branch=master"

PREFERRED_VERSION_python3-chipscopy = "2022.2.1670292617"
#PREFERRED_VERSION_python3-chipscopy = "2024.1.1717799899"

SRCREV = "${AUTOREV}"
PV = "1.0+git${SRCPV}"

S = "${WORKDIR}/git"

do_install() {
        install -d ${D}${bindir}/ez-hw-test
        cp -r --no-preserve=ownership ${S}/* -t ${D}${bindir}/ez-hw-test
}

FILES:${PN} += "${bindir}/ez-hw-test/*"
