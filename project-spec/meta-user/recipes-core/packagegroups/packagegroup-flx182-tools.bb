SUMMARY = "Test utilities for FLX-182"
DESCRIPTION = "Utility packages to help testing the Versal-based \
devices developed for FELIX phase2 upgrade"

PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

FLX182_PACKAGES = " \
        ethtool \
        phytool \
        netcat \
        i2c-tools \
        fio \
        lmbench \        
        iperf3 \
        socat \
        memtester \
        libgpiod \
        libgpiod-tools \
        packagegroup-petalinux-lmsensors \
        git \
        wget \
        curl \
"

RDEPENDS:${PN}:append = " \
        ${FLX182_PACKAGES} \
        "
