DESCRIPTION = "Rich is a Python library for rich text and beautiful formatting in the terminal."
HOMEPAGE = "https://pypi.org/project/rich/"
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://LICENSE;md5=d0d35d5357392e5bfeb0d0a7e6ba4d83"

SRC_URI[sha256sum] = "d59e94a0e3e686f0d268fe5c7060baa1bd6744abca71b45351f5850a3aaa6764"

inherit pypi setuptools3

RDEPENDS:${PN} += "\
		${PYTHON_PN}-colorama \
		${PYTHON_PN}-commonmark \
		${PYTHON_PN}-typing-extensions \
		"

		# ${PYTHON_PN}-dataclasses \
		# ${PYTHON_PN}-pygments \
		# ${PYTHON_PN}-ipywidgets \