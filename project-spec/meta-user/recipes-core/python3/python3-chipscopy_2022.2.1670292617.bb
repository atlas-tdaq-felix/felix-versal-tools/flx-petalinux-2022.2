DESCRIPTION = "Open-source project from Xilinx that enables high-level control of Versal debug IP running in hardware"
HOMEPAGE = "https://pypi.org/project/chipscopy/2022.2.1670292617/"
LICENSE = "Apache-2.0"

LIC_FILES_CHKSUM = "file://LICENSE;md5=e044f1626fcb471118a71a253d550cb1"

SRC_URI[sha256sum] = "9209d5f8e00c5774c96d954f32ba599e06ef1452655eb3a550decb97f7e81884"

inherit pypi setuptools3

RDEPENDS:${PN} += "\
		${PYTHON_PN}-click \
		${PYTHON_PN}-more-itertools \
		${PYTHON_PN}-rich \
		${PYTHON_PN}-loguru \
		${PYTHON_PN}-matplotlib \
		${PYTHON_PN}-typing-extensions \		
        ${PYTHON_PN}-image \
        ${PYTHON_PN}-antlr4-python3-runtime \
		"
