DESCRIPTION = "commonmark.py is a pure Python port of jgm’s commonmark.js, a Markdown parser and renderer for the CommonMark specification, using only native modules."
HOMEPAGE = "https://pypi.org/project/rich/"
LICENSE = "BSD-3-Clause"

LIC_FILES_CHKSUM = "file://LICENSE;md5=37e127eb75a030780aefcfc584e78523"

SRC_URI[sha256sum] = "452f9dc859be7f06631ddcb328b6919c67984aca654e5fefb3914d54691aed60"

inherit pypi setuptools3
