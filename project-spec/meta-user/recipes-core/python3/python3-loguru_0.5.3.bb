DESCRIPTION = "Loguru is a library which aims to bring enjoyable logging in Python."
HOMEPAGE = "https://pypi.org/project/loguru/"
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://LICENSE;md5=17a1d3575545a1e1c7c7f835388beafe"

SRC_URI[sha256sum] = "b28e72ac7a98be3d28ad28570299a393dfcd32e5e3f6a353dec94675767b6319"

inherit pypi setuptools3
