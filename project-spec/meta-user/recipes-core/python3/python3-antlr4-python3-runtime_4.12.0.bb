DESCRIPTION = "Loguru is a library which aims to bring enjoyable logging in Python."
HOMEPAGE = "https://pypi.org/project/loguru/"
LICENSE = "CLOSED"

SRC_URI[sha256sum] = "0a8b82f55032734f43ed6b60b8a48c25754721a75cd714eb1fe9ce6ed418b361"

inherit pypi setuptools3
