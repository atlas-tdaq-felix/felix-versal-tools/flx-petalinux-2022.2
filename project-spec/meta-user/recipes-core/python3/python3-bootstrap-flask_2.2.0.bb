DESCRIPTION = "Bootstrap-Flask is a collection of Jinja macros \
	for Bootstrap 4 & 5 and Flask. It helps you to render Flask-related \
	objects and data to Bootstrap HTML more easily."
	
HOMEPAGE = "https://pypi.org/project/Bootstrap-Flask/"
PYPI_PACKAGE = "Bootstrap-Flask"
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://LICENSE;md5=4891553f23c9acba2e879e0ec7db94e7"

SRC_URI[sha256sum] = "40f3bf7d5802e429b2fca286a548bdde816c1185c28aa8df3051cff35cd05cac"

inherit pypi setuptools3
