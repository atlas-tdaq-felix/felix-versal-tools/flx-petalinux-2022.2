DESCRIPTION = "Open-source project from Xilinx that enables high-level control of Versal debug IP running in hardware"
HOMEPAGE = "https://pypi.org/project/chipscopy/2021.1.1637713037/"
LICENSE = "Apache-2.0"

LIC_FILES_CHKSUM = "file://LICENSE;md5=e044f1626fcb471118a71a253d550cb1"

SRC_URI[sha256sum] = "fb0561916782a710de7c0692738d2175aa2ca14aa62ce24353b79fe88470bcba"

inherit pypi setuptools3

RDEPENDS:${PN} += "\
		${PYTHON_PN}-click \
		${PYTHON_PN}-more-itertools \
		${PYTHON_PN}-rich \
		${PYTHON_PN}-loguru \
		${PYTHON_PN}-matplotlib \
		${PYTHON_PN}-typing-extensions \		
        ${PYTHON_PN}-image \
		"
